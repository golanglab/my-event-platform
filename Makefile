SHELL := /bin/bash

TOPDIR := $(shell pwd)
MONGO_CONTAINER_NAME := mymongo
RABBITMQ_CONTAINER_NAME := rabbitmq

# Ensure help is the default goal.all:
help:

.PHONY: help

help:
	@echo "See a source code"


start-mongo:
	docker run -d -p 27017-27019:27017-27019 --name $(MONGO_CONTAINER_NAME) \
	-v $(TOPDIR)/test/mongo:/docker-entrypoint-initdb.d \
	-e MONGO_INITDB_DATABASE=myevents mongo

stop-mongo:
	docker stop $(MONGO_CONTAINER_NAME)
	docker rm $(MONGO_CONTAINER_NAME)

start-rabbitmq:
	docker run --detach  --name $(RABBITMQ_CONTAINER_NAME) -p 5672:5672  -p 15672:15672 rabbitmq:3-management

stop-rabbitmq:
	docker stop $(RABBITMQ_CONTAINER_NAME)
	docker rm $(RABBITMQ_CONTAINER_NAME)

test-mongolayer:
	@$(MAKE) start-mongo
	go test -v ./pkg/persistence/mongolayer
	@$(MAKE) stop-mongo

test-listener:
	go clean -testcache; go test -v ./pkg/msgqueue/amqp -run TestListenerNoEvent
