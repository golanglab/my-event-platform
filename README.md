# My Event Platform

Based on the book Cloud Native programming with Golang. https://learning.oreilly.com/library/view/cloud-native-programming/9781787125988/

Book's source code: https://github.com/PacktPublishing/Cloud-Native-programming-with-Golang

![ERP](./docs/diagrams/erp.drawio.svg)