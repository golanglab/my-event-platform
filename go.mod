module my-event-platform

go 1.15

require (
	github.com/Shopify/sarama v1.29.1
	github.com/gorilla/mux v1.8.0
	github.com/mitchellh/mapstructure v1.4.1
	github.com/streadway/amqp v1.0.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
