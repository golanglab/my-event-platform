package main

import (
	"flag"
	"fmt"
	"github.com/streadway/amqp"
	"my-event-platform/pkg/bookingservice"
	"my-event-platform/pkg/configuration"
	"my-event-platform/pkg/msgqueue"
	msgqueue_amqp "my-event-platform/pkg/msgqueue/amqp"
	"my-event-platform/pkg/persistence/dblayer"
)

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {

	var eventListener msgqueue.EventListener
	var eventEmitter msgqueue.EventEmitter

	confPath := flag.String("conf", `.\config.json`, "flag to set the path to the configuration json file")
	flag.Parse()
	fmt.Println(*confPath)
	config, _ := configuration.ExtractConfiguration(*confPath)
	fmt.Println(config)

	fmt.Println("Connecting to database...")
	dbhandler, err := dblayer.NewPersistenceLayer(config.Databasetype, config.DBConnection)
	panicIfErr(err)

	switch config.MessageBrokerType {
	case "amqp":
		fmt.Println("Connecting to RabbitMQ...")
		conn, err := amqp.Dial(config.AMQPMessageBroker)
		panicIfErr(err)

		eventListener, err = msgqueue_amqp.NewAMQPEventListener(conn, "events", "booking")
		panicIfErr(err)

		eventEmitter, err = msgqueue_amqp.NewAMQPEventEmitter(conn, "events")
		panicIfErr(err)

	default:
		panic("Bad message broker type: " + config.MessageBrokerType)
	}

	processor := bookingservice.EventProcessor{eventListener, dbhandler}

	go processor.ProcessEvents()

	fmt.Println("Starting ServeAPI")
	bookingservice.ServeAPI(config.RestfulEndpoint, dbhandler, eventEmitter)
}
