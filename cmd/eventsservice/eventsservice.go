package main

import (
	"flag"
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"my-event-platform/pkg/configuration"
	"my-event-platform/pkg/eventsservice"
	msgqueue_amqp "my-event-platform/pkg/msgqueue/amqp"
	"my-event-platform/pkg/persistence/dblayer"
)

func main() {
	confPath := flag.String("conf", `.\config.json`, "flag to set the path to the configuration json file")
	flag.Parse()
	fmt.Println(*confPath)
	config, _ := configuration.ExtractConfiguration(*confPath)
	fmt.Println(config)

	fmt.Println("Connecting to database...")
	dbhandler, err := dblayer.NewPersistenceLayer(config.Databasetype, config.DBConnection)
	if err != nil {
		panic(err)
	}

	fmt.Println("Connecting to RabbitMQ...")
	conn, err := amqp.Dial(config.AMQPMessageBroker)
	if err != nil {
		panic(err)
	}

	emitter, err := msgqueue_amqp.NewAMQPEventEmitter(conn, "events")
	if err != nil {
		panic(err)
	}

	//RESTful API start
	fmt.Println("Starting Events Service...")
	log.Fatal(eventsservice.ServeAPI(config.RestfulEndpoint, dbhandler, emitter))
}
