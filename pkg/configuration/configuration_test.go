package configuration_test

import (
	"fmt"
	"my-event-platform/pkg/configuration"
	"testing"
)

func TestExtractConfiguration(t *testing.T) {
	conf, err := configuration.ExtractConfiguration("./test/config.json")

	if err != nil {
		t.Error(err)
	}

	wantDatabasetype := "mongodb"
	if string(conf.Databasetype) != wantDatabasetype {
		t.Errorf("conf.Databasetype = %s;  want %s ", conf.Databasetype, wantDatabasetype)
	}

	wantDBConnection := "mongodb://127.0.0.1"
	if conf.DBConnection != wantDBConnection {
		t.Errorf("conf.DBConnection = %s;  want %s ", conf.DBConnection, wantDBConnection)
	}

	wantRestfulEndpoint := "localhost:8181"
	if conf.RestfulEndpoint != wantRestfulEndpoint {
		t.Errorf("conf.RestfulEndpoint = %s;  want %s ", conf.RestfulEndpoint, wantRestfulEndpoint)
	}

	wantMessageBrokerType := "amqp"
	if string(conf.MessageBrokerType) != wantMessageBrokerType {
		t.Errorf("conf.MessageBrokerType = %s;  want %s ", conf.MessageBrokerType, wantMessageBrokerType)
	}

	wantAMQPMessageBroker := "amqp://guest:guest@localhost:5672"
	if string(conf.AMQPMessageBroker) != wantAMQPMessageBroker {
		t.Errorf("conf.AMQPMessageBroker = %s;  want %s ", conf.AMQPMessageBroker, wantAMQPMessageBroker)
	}

	fmt.Println(conf)

}

func TestExtractConfigurationDefault(t *testing.T) {
	conf, err := configuration.ExtractConfiguration("./test/foobar.json")

	if err.Error() != "open ./test/foobar.json: no such file or directory" && err != nil {
		t.Error(err.Error())
	}

	wantDatabasetype := "mongodb"
	if string(conf.Databasetype) != wantDatabasetype {
		t.Errorf("conf.Databasetype = %s;  want %s ", conf.Databasetype, wantDatabasetype)
	}

	wantDBConnection := "mongodb://127.0.0.1"
	if conf.DBConnection != wantDBConnection {
		t.Errorf("conf.DBConnection = %s;  want %s ", conf.DBConnection, wantDBConnection)
	}

	wantRestfulEndpoint := "localhost:8181"
	if conf.RestfulEndpoint != wantRestfulEndpoint {
		t.Errorf("conf.RestfulEndpoint = %s;  want %s ", conf.RestfulEndpoint, wantRestfulEndpoint)
	}

	wantMessageBrokerType := "amqp"
	if string(conf.MessageBrokerType) != wantMessageBrokerType {
		t.Errorf("conf.MessageBrokerType = %s;  want %s ", conf.MessageBrokerType, wantMessageBrokerType)
	}

	wantAMQPMessageBroker := "amqp://guest:guest@localhost:5672"
	if string(conf.AMQPMessageBroker) != wantAMQPMessageBroker {
		t.Errorf("conf.AMQPMessageBroker = %s;  want %s ", conf.AMQPMessageBroker, wantAMQPMessageBroker)
	}

	fmt.Println(conf)

}
