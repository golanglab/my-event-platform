package persistence

type DatabaseHandler interface {
	AddBookingForUser([]byte, Booking) error
	FindUser(string, string) (User, error)
	AddEvent(Event) ([]byte, error)
	FindEvent([]byte) (Event, error)
	FindEventByName(string) (Event, error)
	FindAllAvailableEvents() ([]Event, error)
}
