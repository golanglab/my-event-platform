package mongolayer

import (
	"fmt"
	"my-event-platform/pkg/persistence"
	"testing"
)

func TestNewMongoDBLayer(t *testing.T) {
	databasehandler, err := NewMongoDBLayer("mongodb://127.0.0.1")

	if err != nil {
		t.Fatal(err)
	}

	if databasehandler == nil {
		t.Fatal(err)
	}
}

func TestFindEvent(t *testing.T) {
	databasehandler, err := NewMongoDBLayer("mongodb://127.0.0.1")

	if err != nil {
		t.Fatal(err)
	}

	if databasehandler == nil {
		t.Fatal(err)
	}

	event, err := databasehandler.FindEventByName("opera aida")

	if err != nil {
		t.Error(err)
	}

	wantEventName := "opera aida"

	if event.Name != wantEventName {
		t.Errorf("Event Name = %s;  want %s ", event.Name, wantEventName)
	}

	//Now I can use event.ID to test find event by ID
	event, err = databasehandler.FindEvent([]byte(event.ID))

	if err != nil {
		t.Error(err)
	}

	if event.Name != wantEventName {
		t.Errorf("Event Name = %s;  want %s ", event.Name, wantEventName)
	}

	event, err = databasehandler.FindEventByName("not existing")

	if err.Error() != "not found" {
		t.Error(err)
	}

	wantEventName = ""

	if event.Name != wantEventName {
		t.Errorf("Event Name = %s;  want %s ", event.Name, wantEventName)
	}
}

func TestFindAllEvents(t *testing.T) {
	databasehandler, err := NewMongoDBLayer("mongodb://127.0.0.1")

	if err != nil {
		t.Fatal(err)
	}

	if databasehandler == nil {
		t.Fatal(err)
	}

	events, err := databasehandler.FindAllAvailableEvents()

	for _, event := range events {
		fmt.Println(event.Name)
	}
}

func TestAddEvent(t *testing.T) {
	e := persistence.Event{}

	e.Name = "newly added"
	e.StartDate = 1625925049
	e.EndDate = 1626357049
	e.Duration = 120

	databasehandler, err := NewMongoDBLayer("mongodb://127.0.0.1")

	if err != nil {
		t.Fatal(err)
	}

	if databasehandler == nil {
		t.Fatal(err)
	}

	id, err := databasehandler.AddEvent(e)

	if err != nil {
		t.Fatal(err)
	}

	//fmt.Println(bson.ObjectId(id))

	//Now I can use event.ID to test find event by ID
	event, err := databasehandler.FindEvent(id)

	if err != nil {
		t.Error(err)
	}

	wantEventName := e.Name
	if event.Name != wantEventName {
		t.Errorf("Event Name = %s;  want %s ", event.Name, wantEventName)
	}
}
