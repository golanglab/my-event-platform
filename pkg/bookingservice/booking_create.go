package bookingservice

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"my-event-platform/pkg/contracts"
	"my-event-platform/pkg/msgqueue"
	"my-event-platform/pkg/persistence"
	"net/http"
	"time"
)

type CreateBookingHandler struct {
	eventEmitter msgqueue.EventEmitter
	database     persistence.DatabaseHandler
}

type createBookingRequest struct {
	Seats int `json:"seats"`
}

func (h *CreateBookingHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	//fmt.Println("Inside SErveHTTP")
	routeVars := mux.Vars(r)
	eventID, ok := routeVars["eventID"]
	if !ok {
		w.WriteHeader(400)
		fmt.Fprint(w, "missing route parameter 'eventID'")
		return
	}
	//fmt.Fprint(w, "eventID: ", eventID )

	eventIDMongo, _ := hex.DecodeString(eventID)

	event, err := h.database.FindEvent(eventIDMongo)
	if err != nil {
		w.WriteHeader(404)
		fmt.Fprintf(w, "event %s could not be loaded: %s", eventID, err)
		return
	}
	//fmt.Println(" evenvName:", event.Name)
	//fmt.Println(" evenvName:", event.ID)

	bookingRequest := createBookingRequest{}
	err = json.NewDecoder(r.Body).Decode(&bookingRequest)
	if err != nil {
		w.WriteHeader(400)
		fmt.Fprintf(w, "could not decode JSON body: %s", err)
		return
	}

	//fmt.Fprint(w, "  Seats: ", bookingRequest.Seats )

	if bookingRequest.Seats <= 0 {
		w.WriteHeader(400)
		fmt.Fprintf(w, "seat number must be positive (was %d)", bookingRequest.Seats)
		return
	}

	eventIDAsBytes, _ := event.ID.MarshalText()
	booking := persistence.Booking{
		Date:    time.Now().Unix(),
		EventID: eventIDAsBytes,
		Seats:   bookingRequest.Seats,
	}

	msg := contracts.EventBookedEvent{
		EventID: event.ID.Hex(),
		UserID:  "someUserID",
	}

	//fmt.Println("Emitter: ", h.eventEmitter)
	err = h.eventEmitter.Emit(&msg)
	if err != nil {
		panic(err)
	}

	user, err := h.database.FindUser("Dima", "Mirkar")
	//fmt.Println(user.ID)
	err = h.database.AddBookingForUser([]byte(user.ID), booking)
	if err != nil {
		fmt.Println("AddBookingForUser error: ", err)
	}

	//fmt.Println("msg ", msg)
	//fmt.Println("booking ", booking)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(201)

	json.NewEncoder(w).Encode(&booking)

}
