package bookingservice

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/streadway/amqp"
	msgqueue_amqp "my-event-platform/pkg/msgqueue/amqp"
	"my-event-platform/pkg/persistence/dblayer"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestCreateBookingHandler(t *testing.T) {

	dbhandler, _ := dblayer.NewPersistenceLayer(dblayer.MONGODB, "mongodb://127.0.0.1")

	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672")
	eventEmitter, err := msgqueue_amqp.NewAMQPEventEmitter(conn, "events")

	myjson := "{\"seats\": 3}"

	// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
	// pass 'nil' as the third parameter.
	req, err := http.NewRequest("POST", "/events/60e7445b25fbffb123070474/bookings", strings.NewReader(string(myjson)))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	router := mux.NewRouter()
	handler := &CreateBookingHandler{eventEmitter: eventEmitter, database: dbhandler}
	router.Handle("/events/{eventID}/bookings", handler)
	router.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	fmt.Println(rr.Body.String())
}
