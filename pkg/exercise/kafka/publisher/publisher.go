package main

import (
	"fmt"
	"github.com/Shopify/sarama"
	"log"
	"os"
	"strings"
)

func main() {
	brokerList := os.Getenv("KAFKA_BROKERS")
	if brokerList == "" {
		brokerList = "localhost:29092"
	}

	brokers := strings.Split(brokerList, ",")
	config := sarama.NewConfig()
	config.Producer.Return.Successes = true

	client, err := sarama.NewClient(brokers, config)

	if err != nil {
		panic(err)
	}

	producer, err := sarama.NewSyncProducerFromClient(client)
	if err != nil {
		panic(err)
	}
	defer producer.Close()

	i := 44
	text := fmt.Sprintf("message %08d", i)

	topic := "myfirsttopic"
	partition, offset, err := producer.SendMessage(&sarama.ProducerMessage{Topic: topic, Key: nil, Value: sarama.StringEncoder(text)})
	if err != nil {
		log.Fatalf("unable to produce message: %q", err)
	}

	fmt.Println("Partition: ", partition)
	fmt.Println("Offset: ", offset)

	msg := &sarama.ProducerMessage{
		Topic: "myevent",
		Value: sarama.ByteEncoder("{ \"value\": 77 }"),
	}

	partition, offset, err = producer.SendMessage(msg)
	if err != nil {
		log.Fatalf("unable to produce message: %q", err)
	}

	fmt.Println("Partition: ", partition)
	fmt.Println("Offset: ", offset)

}
