package main

import (
	"fmt"
	"github.com/Shopify/sarama"
	"log"
	"os"
	"strings"
	"time"
)

func main() {
	brokerList := os.Getenv("KAFKA_BROKERS")
	if brokerList == "" {
		brokerList = "localhost:29092"
	}

	brokers := strings.Split(brokerList, ",")
	config := sarama.NewConfig()
	config.Producer.Return.Successes = true

	client, err := sarama.NewClient(brokers, config)

	if err != nil {
		panic(err)
	}

	consumer, err := sarama.NewConsumerFromClient(client)
	if err != nil {
		panic(err)
	}

	topic := "myevent"

	partitions, err := consumer.Partitions(topic)

	for _, partition := range partitions {
		log.Printf("consuming partition %s:%d", topic, partition)
		pConsumer, err := consumer.ConsumePartition(topic, partition, 0)
		if err != nil {
			panic(err)
		}
		go func() {
			for msg := range pConsumer.Messages() {
				log.Printf("received message %v", string(msg.Value))
			}
		}()
	}

	fmt.Println(partitions)
	time.Sleep(5 * time.Second)

	/*	for _, partition := range partitions {
		log.Printf("consuming partition %s:%d", topic, partition)
		pConsumer, err := consumer.ConsumePartition(topic, partition, 0)
		if err != nil {
			panic(err)
		}
		for stay, timeout := true, time.After(20*time.Second); stay; {
			select {
			case msg := <- pConsumer.Messages():
				log.Printf("received message %v", string(msg.Value))
			case <-timeout:
				fmt.Println("timed out")
				stay = false
			default:
			}

		}
			}*/
}
