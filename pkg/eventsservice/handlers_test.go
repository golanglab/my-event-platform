package eventsservice

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/streadway/amqp"
	msgqueue_amqp "my-event-platform/pkg/msgqueue/amqp"
	"my-event-platform/pkg/persistence"
	"my-event-platform/pkg/persistence/dblayer"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestAllEventHandler(t *testing.T) {

	dbhandler, _ := dblayer.NewPersistenceLayer(dblayer.MONGODB, "mongodb://127.0.0.1")

	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672")
	emitter, err := msgqueue_amqp.NewAMQPEventEmitter(conn, "events")
	eventHandler := NewEventHandler(dbhandler, emitter)

	// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
	// pass 'nil' as the third parameter.
	req, err := http.NewRequest("GET", "/events", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(eventHandler.AllEventHandler)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	/*	expected := "I am AllEventHandler"
		if rr.Body.String() != expected {
			t.Errorf("handler returned unexpected body: got %v want %v",
				rr.Body.String(), expected)
		}*/

	//fmt.Println(rr.Body.String())

	var dat = []persistence.Event{}
	json.Unmarshal([]byte(rr.Body.String()), &dat)

	if len(dat) != 2 {
		t.Errorf("handler returned wrong number of events: got %v want 2", len(dat))
	}
}

func TestFindEventHandler(t *testing.T) {

	dbhandler, _ := dblayer.NewPersistenceLayer(dblayer.MONGODB, "mongodb://127.0.0.1")

	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672")
	emitter, err := msgqueue_amqp.NewAMQPEventEmitter(conn, "events")
	eventHandler := NewEventHandler(dbhandler, emitter)

	req, err := http.NewRequest("GET", "/events/name/opera%20aida", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	// Need to create a router that we can pass the request through so that the vars will be added to the context
	router := mux.NewRouter()
	router.HandleFunc("/events/{SearchCriteria}/{search}", eventHandler.FindEventHandler)
	router.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	//fmt.Println(rr.Body.String())

	var dat = persistence.Event{}

	json.Unmarshal([]byte(rr.Body.String()), &dat)

	fmt.Println(dat.ID.Hex())

	urlID := fmt.Sprintf("/events/id/%s", dat.ID.Hex())
	//fmt.Println(urlID)

	req, err = http.NewRequest("GET", urlID, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr.Body.Reset()
	router.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	fmt.Println(rr.Body.String())
}

func TestNewEventHandler(t *testing.T) {
	e := persistence.Event{}

	e.Name = "newly added"
	e.StartDate = 1625925049
	e.EndDate = 1626357049
	e.Duration = 120

	myjson, err := json.Marshal(e)
	if err != nil {
		t.Error("Failed marshall")
	}

	//str := strings.NewReader(string(myjson))

	//io.Copy(os.Stdout, str)

	dbhandler, _ := dblayer.NewPersistenceLayer(dblayer.MONGODB, "mongodb://127.0.0.1")

	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672")
	emitter, err := msgqueue_amqp.NewAMQPEventEmitter(conn, "events")
	eventHandler := NewEventHandler(dbhandler, emitter)

	// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
	// pass 'nil' as the third parameter.
	req, err := http.NewRequest("POST", "/events", strings.NewReader(string(myjson)))
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(eventHandler.NewEventHandler)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	fmt.Println(rr.Body.String())
}
