package amqp

import (
	"fmt"
	"github.com/streadway/amqp"
	"my-event-platform/pkg/contracts"
	"testing"
	"time"
)

func TestListener(t *testing.T) {
	connection, err := amqp.Dial("amqp://guest:guest@localhost:5672")
	if err != nil {
		panic("could not establish AMQP connection: " + err.Error())
	}

	eventListener, err := NewAMQPEventListener(connection, "events", "myevents")

	events, errors, err := eventListener.Listen("eventCreated")

	_ = events

	/*	for err := range errors {
		fmt.Println(err)
		fmt.Println("Inside")
	}*/

	for stay, timeout := true, time.After(10*time.Second); stay; {
		select {
		case err_out := <-errors:
			fmt.Println("Error: ", err_out)
		case event := <-events:
			fmt.Println("Event: ", event)
		case <-timeout:
			fmt.Println("timed out")
			stay = false
		default:
		}
	}
}

func TestListenerNoEvent(t *testing.T) {
	connection, err := amqp.Dial("amqp://guest:guest@localhost:5672")
	if err != nil {
		panic("could not establish AMQP connection: " + err.Error())
	}

	eventListener, err := NewAMQPEventListener(connection, "events", "myevents")

	events, errors, err := eventListener.Listen("eventNotExist", "eventCreated")

	_ = events

	/*	for err := range errors {
		fmt.Println(err)
		fmt.Println("Inside")
	}*/

	for stay, timeout := true, time.After(20*time.Second); stay; {
		select {
		case err_out := <-errors:
			fmt.Println("Error: ", err_out)
		case event := <-events:
			fmt.Println("Name: ", event.EventName())
			fmt.Printf("(%v, %T)\n", event, event)
			switch v := event.(type) {
			case *contracts.EventCreatedEvent:
				fmt.Println("ID: ", v.ID)
				fmt.Println("Name: ", v.Name)
			default:
				fmt.Println("Unknown event type")

			}
		case <-timeout:
			fmt.Println("timed out")
			stay = false
		default:
		}
	}
}
