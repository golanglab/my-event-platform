package kafka

import (
	"github.com/Shopify/sarama"
	"my-event-platform/pkg/contracts"
	"my-event-platform/pkg/msgqueue"
	"testing"
	"time"
)

func TestKafkaEmitter(t *testing.T) {
	var eventEmitter msgqueue.EventEmitter

	brokers := []string{"localhost:29092"}

	config := sarama.NewConfig()
	config.Producer.Return.Successes = true

	conn, err := sarama.NewClient(brokers, config)

	if err != nil {
		panic(err)
	}

	eventEmitter, err = NewKafkaEventEmitter(conn)
	if err != nil {
		panic(err)
	}

	e := &contracts.EventCreatedEvent{
		ID:         "12345",
		Name:       "somename",
		LocationID: "893775",
		Start:      time.Now().Add(time.Hour * 20),
		End:        time.Now().Add(time.Hour * 60),
	}

	eventEmitter.Emit(e)
}
